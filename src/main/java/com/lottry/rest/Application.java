package com.lottry.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public RepositoryRestConfigurer repostoryRestConfigurer() {
        return RepositoryRestConfigurer.withConfig(config -> {
            config.getCorsRegistry().addMapping("/people/**")
                .allowedOrigins("http://localhost:3000", 
                                "https://react.luno89.usw1.kubesail.io/");
        });
    }
}
