package com.lottry.rest;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.boot.test.web.client.TestRestTemplate;

import com.lottry.rest.domain.Person;

import static org.junit.Assert.*;
import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ApplicationTest {
    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    @DisplayName("Application Loads")
    public void contextLoads() {
        assertTrue(true);
    }    
    
    @Test
    @DisplayName("response returns people endpoint")
    public void personExists() {
        String jsonBlob = this.restTemplate.getForObject("http://localhost:" + port + "/", String.class);

        assertThat(jsonBlob).contains("/people");
    }    
    
    @Test
    @DisplayName("response saves people and returns one")
    public void savePeople() {

        Person person = new Person();
        person.setFirstName("first");
        person.setLastName("last");
        person.setEmail("someone@email.com");
        
        Person person2 = new Person();
        person.setFirstName("first2");
        person.setLastName("last");
        person.setEmail("someone@email.com");

        this.restTemplate.postForEntity("http://localhost:" + port + "/people", person, Person.class);
        this.restTemplate.postForEntity("http://localhost:" + port + "/people", person2, Person.class);

        String jsonBlob = this.restTemplate.getForObject("http://localhost:" + port + "/people", String.class);

        assertThat(jsonBlob).contains("first");
        assertThat(jsonBlob).contains("first2");
    }
}